import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  grocery: string;
  editedGrocery : string;
  groceries = [];
  id = 1;
  show = false;

  onClick(){
    this.groceries.push({name: this.grocery, id: this.id, show: false});
    this.id += 1;
    this.grocery = '';
  }

  onDelete(item){
    for(var i = 0; i< this.groceries.length; i++){
      if(item.id == this.groceries[i].id){
        this.groceries.splice(i,1)
        break;
      }
    }
  }

  onEdit(item){
    for(var i = 0; i< this.groceries.length; i++){
      if(item.id == this.groceries[i].id){
        this.groceries.splice(i,1, {name: this.editedGrocery, id: this.id})
        break;
      }
    }
  }

  showEditInput(item){
    item.show = !item.show;
  }
}
